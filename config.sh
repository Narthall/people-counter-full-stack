#!/bin/bash
echo "Attention, une fois la procédure lancée, il faut aller au bout (ou la relancer en cas d'erreur)."
echo "Les changements apportés aux streams surveillés actuellement seront perdus."
echo "Vous pouvez aussi modifier manuellement le fichier situé dans './app/bin/yaml/parameters.py'"
read -p "Êtes-vous sûr(e) de lancer la procédure automatique ? (O=Oui)`echo $'\n> '`" REPLY
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Oo]$ ]]
then
    # Ici, on prépare le futur fichier de paramètres
    echo "# QUELLES SONT LES ADRESSES DES FLUX CAMERAS ?" > ./app/bin/yaml/parameters.py
    echo "# ET QUELLES SONT LES DIRECTIONS D'ENTREE DES PERSONNES ?" >> ./app/bin/yaml/parameters.py
    echo "# EXEMPLE :" >> ./app/bin/yaml/parameters.py
    echo "# streams = {" >> ./app/bin/yaml/parameters.py
    echo "#   'rtsp://tartempion...': 'haut'," >> ./app/bin/yaml/parameters.py
    echo "#   'fichier_video.mp4': 'bas'," >> ./app/bin/yaml/parameters.py
    echo "#   'https://toto...': 'gauche'," >> ./app/bin/yaml/parameters.py
    echo "#   'http://tatin...': 'droite'" >> ./app/bin/yaml/parameters.py
    echo "# }" >> ./app/bin/yaml/parameters.py
    echo "streams = {" >> ./app/bin/yaml/parameters.py

    while true
    do
        read -p "Quelle est l'adresse du stream que vous voulez surveiller ?`echo $'\n> '`" stream
        while true
        do
            read -p "Dans quelle direction les personnes entreront-elles ? ('haut', 'bas', 'gauche', 'droite')`echo $'\n> '`" direction
            case "$direction" in
                "haut"|"bas"|"gauche"|"droite") break;;
                *) echo "Direction invalide.";;
            esac
        done
        echo -e "\t\"$stream\": \"$direction\"," >> ./app/bin/yaml/parameters.py
        read -r -p "Voulez-vous ajouter un nouveau stream ? (O=Oui/N=Non)`echo $'\n> '`" keepon
        case "$keepon" in
            "o"|"O") echo   ;;
            "n"|"N") break;;
        esac
    done
    echo -e "\t}" >> ./app/bin/yaml/parameters.py
else
    exit 1
fi

./reset-config.sh

./start.sh