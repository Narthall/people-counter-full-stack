# QUELLES SONT LES ADRESSES DES FLUX CAMERAS ?
# ET QUELLES SONT LES DIRECTIONS D'ENTREE DES PERSONNES ?
# EXEMPLE :
# streams = {
#   'rtsp://tartempion...': 'haut',
#   'fichier_video.mp4': 'bas',
#   'https://toto...': 'gauche',
#   'http://tatin...': 'droite'
# }
streams = {
	"videos/walkingman.mp4": "gauche",
	"videos/walkingman_copy.mp4": "droite",
	}
