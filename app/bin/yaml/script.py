import parameters as params
import subprocess
import sys
import fileinput

def inplace_change(filename, old_string, new_string):
    # Safely read the input filename using 'with'
    with open(filename) as f:
        s = f.read()
        if old_string not in s:
            print('"{old_string}" not found in {filename}.'.format(**locals()))
            return

    # Safely write the changed content, if found in the file
    with open(filename, 'w') as f:
        print('Changing "{old_string}" to "{new_string}" in {filename}'.format(**locals()))
        s = s.replace(old_string, new_string)
        f.write(s)

camera_id = 0
distant_stream = ""
v_or_h_line = ""
entering_direction = ""
commands = []

# Ces lignes remettent les fichiers .enf et docker-compose à zéro
commands.append("cat /workdir/base.env > /workdir/.env")
commands.append("cat /workdir/base.docker-compose.yml > /workdir/docker-compose.yml")

for ds, dn in params.streams.items():
    distant_stream = ds
    if dn == "haut" or dn == "bas":
        v_or_h_line = "horizontal"
        if dn == "haut":
            entering_direction = "up"
        elif dn == "bas":
            entering_direction = "down"
    elif dn == "gauche" or dn == "droite":
        v_or_h_line = "vertical"
        if dn == "gauche":
            entering_direction = "left"
        elif dn == "droite":
            entering_direction = "right"
    else:
        print("[ERREUR] La direction du flux '" + ds + "' est incorrecte !")
        sys.exit()

    # Ces lignes servent à ajouter les lignes nécessaires au fichier .env
    commands.append("echo '\nCAMERA_ID_" + str(camera_id) + "=" + str(camera_id) + "' >> /workdir/.env")
    commands.append("echo 'ENTERING_DIRECTION_" + str(camera_id) + "=" + str(entering_direction) + "' >> /workdir/.env")
    commands.append("echo 'V_OR_H_LINE_" + str(camera_id) + "=" + str(v_or_h_line) + "' >> /workdir/.env")
    commands.append("echo 'DISTANT_STREAM_" + str(camera_id) + "=" + str(distant_stream) + "' >> /workdir/.env")

    
    # Ces lignes servent à ajouter les lignes nécessaires au fichier docker-compose.yml
    commands.append("yq w -i docker-compose.yml services.counter" + str(camera_id) + ".build.context './bin/counter'")
    commands.append("yq w -i docker-compose.yml services.counter" + str(camera_id) + ".container_name '${COMPOSE_PROJECT_NAME" + "}-counter" + str(camera_id) +"'")
    commands.append("yq w -i docker-compose.yml services.counter" + str(camera_id) + ".restart 'always'")
    commands.append("yq w -i docker-compose.yml services.counter" + str(camera_id) + ".volumes[+] './bin/counter:/code'")
    # commands.append("yq w -i docker-compose.yml services.counter" + str(camera_id) + ".environment.CAMERA_ID_" + str(camera_id) + " '${CAMERA_ID_" + str(camera_id) + "}'")
    # commands.append("yq w -i docker-compose.yml services.counter" + str(camera_id) + ".environment.ENTERING_DIRECTION_" + str(camera_id) + " '${ENTERING_DIRECTION_" + str(camera_id) + "}'")
    # commands.append("yq w -i docker-compose.yml services.counter" + str(camera_id) + ".environment.V_OR_H_LINE_" + str(camera_id) + " '${V_OR_H_LINE_" + str(camera_id) + "}'")
    # commands.append("yq w -i docker-compose.yml services.counter" + str(camera_id) + ".environment.DISTANT_STREAM_" + str(camera_id) + " '${DISTANT_STREAM_" + str(camera_id) + "}'")
    commands.append("yq w -i docker-compose.yml services.counter" + str(camera_id) + ".entrypoint '[\"/bin/sh\", \"-c\", \"/usr/bin/xvfb-run -a python people_counter.py --line ${V_OR_H_LINE_" + str(camera_id) + "} --enteringdirection ${ENTERING_DIRECTION_" + str(camera_id) + "} --cameraid ${CAMERA_ID_" + str(camera_id) + "} --input ${DISTANT_STREAM_" + str(camera_id) + "}\"]'")

    print("Camera " + str(camera_id) + " implemented in app/docker-compose.yml")

    camera_id += 1

for command in commands:
    subprocess.call(command, shell=True)

inplace_change("docker-compose.yml", "'[", "[")
inplace_change("docker-compose.yml", "]'", "]")