import pymysql
import cv2
import requests
import os

# Connecting to the database
host = "database"
username = "docker"
password = "docker"
database = "docker"

db = pymysql.connect(host, username, password, database)
# Prepare a cursor object using cursor() method
cursor = db.cursor()

try:
    cursor.execute("""CREATE TABLE IF NOT EXISTS `people_streams` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `camera_id` int(11) NOT NULL,
        `stream_address` varchar(255) NOT NULL,
        `entering_direction` varchar(10) NOT NULL,
        `date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`))
        CHARACTER SET utf8""")
    db.commit()
except Exception as e:
    db.rollback()
    print("[ERROR] " + e)

try:
    cursor.execute("""CREATE TABLE IF NOT EXISTS `people_counter` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `fk_people_streams` int(11) NOT NULL,
        `direction` varchar(10) NOT NULL,
        `date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`))
        CHARACTER SET utf8""")
    db.commit()
except Exception as e:
    db.rollback()
    print("[ERROR] " + e)
    

# Only two authorised actions
class Camera:
    def __init__(self, id, stream_address, entering_direction):
        self.id = int(id)
        self.stream_address = str(stream_address)
        self.entering_direction = str(entering_direction)
        self.create_new_camera()

    def create_new_camera(self):
        sql = """INSERT INTO `people_streams` (
            `id`, `camera_id`, `stream_address`, `entering_direction`, `date`) VALUES (
            NULL,
            """ + str(self.id) + """,
            '""" + self.stream_address + """',
            '""" + self.entering_direction + """',
            CURRENT_TIMESTAMP);"""
        try:
            # Execute the SQL command
            cursor.execute(sql)
            self.stream_id = db.insert_id()
            print("[" + str(self.id) + "][SAVED] New camera " + str(self.id) + " initialised")
        except Exception as e:
            # Rollback in case there is any error
            db.rollback()
            print("[" + str(self.id) + "][ERROR] " + str(e) + ": Camera not saved in database")
            exit()
        # Commit your changes in the database
        db.commit()

    def insert(self, direction, frame):
        if direction != "down" and direction != "up" and direction != "left" and direction != "right":
            print("[" + str(self.id) + "][ERROR] Direction must be up, down, left or right")
            return
        elif direction == self.entering_direction:
            d = "in"
        elif direction == opposite_direction(self.entering_direction):
            d = "out"
        else:
            print("[" + str(self.id) + "][ERROR] Direction must be compatible with chose line type")
            return

        sql = """INSERT INTO `people_counter` (
            `id`, `fk_people_streams`, `direction`, `date`) VALUES (
            NULL,
            """ + str(self.stream_id) + """,
            '""" + d + """',
            CURRENT_TIMESTAMP);"""

        try:
            # Execute the SQL command
            cursor.execute(sql)
            print("[" + str(self.id) + "][SAVED] Object going " + d)
            self.capture(db.insert_id(), frame)
        except Exception as e:
            # Rollback in case there is any error
            db.rollback()
            print("[" + str(self.id) + "][ERROR] " + str(e) + ": Object not saved in database")
            exit()
        # Commit your changes in the database
        db.commit()
        #Capture an image of the camera and send it to webserver

    def capture(self, image_id, frame):
        name = 'output/' + str(image_id) + '.jpg'
        cv2.imwrite(name, frame)
        url = 'http://webserver/api/upload_image.php'
        files = {'file': open(name, 'rb')}
        try:
            response = requests.post(url, files=files)
            print(response.text)
        except Exception as e:
            print("[" + str(self.id) + "][ERROR] " + str(e) + ": Image not sent to webserver")
            exit()
        try:
            os.remove(name)
        except Exception as e:
            print("[" + str(self.id) + "][WARNING] " + str(e) + ": Image not deleted from python directory")


def opposite_direction(direction):
    if direction == "left":
        return "right"
    if direction == "right":
        return "left"
    if direction == "up":
        return "down"
    if direction == "down":
        return "up"