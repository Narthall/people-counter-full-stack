<?php
function prettyPrint($a) {
    echo "<pre>";
    print_r($a);
    echo "</pre>";
}

    
function count_people(string $date_start, string $date_end, string $direction = "all",  int $fk_people_streams = NULL, bool $passed = FALSE) {
    $sql = "SELECT COUNT(*) FROM `people_counter` WHERE (`date` BETWEEN '" . $date_start . "' AND '" . $date_end . "')";
    
    if ($direction === "all") {
        $results = [];
        $directions = ["in", "out"];
        foreach ($directions as $one_direction) {
            $results[$one_direction] = count_people($date_start, $date_end, $one_direction, $fk_people_streams, true);
        }
        return $results;
    }

    
    if (($direction === "in") || ($direction === "out")) {
        $sql = $sql . " AND (`direction` = '" . $direction . "') ";
    } else {
        echo "Wrong Direction.";
        return;
    }

    if ($fk_people_streams != NULL) {
        $sql = $sql . " AND (`fk_people_streams` = '" . $fk_people_streams . "');";
    } else if ($fk_people_streams === NULL) {
        $sql = $sql . ";";
    } else {
        echo "Wrong Camera ID.";
        return;
    }
    
    $count = $GLOBALS['pdo']->query($sql)->fetchColumn();
    if ($passed === TRUE) {
        return $count;
    } else {
        $result[$direction] = $count;
        return $result;
    }
}

function count_hourly(string $date_start, string $date_end, string $direction = "all",  int $fk_people_streams = NULL) {
    $count = "COUNT(*)";
    if ($direction === "all") {
        $count = "FLOOR(COUNT(*)/2)";
    }
    $sql =
    "SELECT
        " . $count . " as `count`,
        GROUP_CONCAT(DISTINCT(
            `people_counter`.`fk_people_streams`)
            SEPARATOR ',') as `fk_people_streams`,
        `people_counter`.`direction` as `direction`,
        `pre`.`date` as `date`
    FROM `people_counter`
    INNER JOIN
    (
        SELECT
            `id`,
            CONCAT(DATE(`date`), ' ', HOUR(`date`), ':00:00') as `date`
        FROM `people_counter`
    ) as `pre`
    ON `people_counter`.`id` = `pre`.`id`
    WHERE (`people_counter`.`date` BETWEEN '" . $date_start . "' AND '" . $date_end . "')";

    if (($direction === "in") || ($direction === "out")) {
        $sql = $sql . " AND (`direction` = '" . $direction . "') ";
    } else if (($direction != "in") && ($direction != "out") && ($direction != "all")) {
        echo "Wrong Direction.";
        return;
    }

    if ($fk_people_streams != NULL) {
        $sql = $sql . " AND (`fk_people_streams` = '" . $fk_people_streams . "') ";
    } else if ($fk_people_streams === NULL) {
        $sql = $sql . "";
    } else {
        echo "Wrong Camera ID.";
        return;
    }
    
    $sql = $sql . "GROUP BY `people_counter`.`direction`, `pre`.`date`
    ORDER BY `pre`.`date` ASC;";
    $count = $GLOBALS['pdo']->query($sql)->fetchAll();
    foreach ($count as $row => $values) {
        if ($direction === "all") {
            $count[$row]['direction'] = "passage";
        }
        $count[$row]['date'] = strtotime($values['date']);
    }
    if ($direction === "all") {
        $direction = "passage";
    }

    $result[$direction] = $count;
    return $result;

}

function translate($direction) {
    switch ($direction) {
        case 'in':
            return "entrée(s)";
        case 'out':
            return "sortie(s)";
        case 'passage':
            return "Passage";
    }
}

function array_replace_key($search, $replace, array $subject) {
    $subject[$replace] = $subject[$search];
    unset($subject[$search]);    

    return $subject;
}