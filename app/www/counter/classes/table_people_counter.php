<?php

class PeopleCounter {
    public $table_name;
    public $id;
    public $fk_people_streams;
    public $direction;
    public $date; // DATE format : YYYY-MM-DD HH:mm:ss
    public $image;
    /**
     * Class constructor.
     */
    public function __construct(int $id, int $fk_people_streams = NULL, string $direction = NULL, string $date = NULL) {
        $this->table_name = "people_counter";
        $this->id = $id;
        if (($fk_people_streams === NULL) || ($direction === NULL) || ($date === NULL)) {
            $pdo = $GLOBALS['pdo'];
            $sql = $pdo->query("SELECT * FROM `" . $this->table_name . "` WHERE `id` = " . $this->id)->fetch();
            $this->fk_people_streams = $sql['fk_people_streams'];
            $this->direction = $sql['direction'];
            $this->date = $sql['date'];
        } else {
            $this->fk_people_streams = $fk_people_streams;
            $this->direction = $direction;
            $this->date = $date;
        }
        $this->image = "/api/uploaded_files/" . $id . ".jpg";
    }
}
