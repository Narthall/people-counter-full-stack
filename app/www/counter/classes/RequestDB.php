<?php

class RequestDB {
    public $date_start;
    public $date_end;
    public $direction;
    public $cameraid;
    public $count;
    public $entries;
    /**
     * Class constructor.
     */
    public function __construct($date_start, $date_end, string $direction="all", int $cameraid = NULL, int $count = NULL, array $entries = []) {
        $this->date_start = $date_start;
        $this->date_end = $date_end;
        $this->direction = $direction;
        $this->cameraid = $cameraid;
        $this->count = $count;
        $this->entries = $entries;
    }

    public function count_people() {
        $sql = "SELECT COUNT(*) FROM `people_counter` WHERE (`date` BETWEEN '" . $this->date_start . "' AND '" . $this->date_end . "')";
        $sql2 =
        "SELECT
            COUNT(*) as `count`,
            `people_counter`.`camera_id` as `camera_id`,
            `people_counter`.`stream` as `stream`,
            `people_counter`.`direction` as `direction`,
            `pre`.`date` as `date`
        FROM `people_counter`
        INNER JOIN
        (
            SELECT
                `id`,
                CONCAT(DATE(`date`), ' ', HOUR(`date`), ':00:00') as `date`
            FROM `people_counter`
        ) as `pre`
        ON `people_counter`.`id` = `pre`.`id`
        WHERE (`people_counter`.`date` BETWEEN '" . $this->date_start . "' AND '" . $this->date_end . "')";

        if ($this->direction === "all") {
            $results = [];
            $directions = ["up", "down", "left", "right"];
            foreach ($directions as $one_direction) {
                $s = new RequestDB($this->date_start, $this->date_end, $one_direction);
                array_push($results, $s->count_people()[0]);
            }
            return $results;
        }

        if (($this->direction === "down") || ($this->direction === "up") || ($this->direction === "left") || ($this->direction === "right")) {
            $sql = $sql . " AND (`direction` = '" . $this->direction . "') ";
            $sql2 = $sql2 . " AND (`direction` = '" . $this->direction . "') ";
        } else {
            echo "Wrong Direction.";
            return;
        }

        if ($this->cameraid != NULL) {
            $sql = $sql . " AND (`camera_id` = '" . $this->cameraid . "');";
            $sql2 = $sql2 . " AND (`camera_id` = '" . $this->cameraid . "')";
        } else if ($this->cameraid === NULL) {
            $sql = $sql . ";";
        } else {
            echo "Wrong Camera ID.";
            return;
        }
        $sql2 = $sql2 . "GROUP BY `people_counter`.`camera_id`, `people_counter`.`stream`, `people_counter`.`direction`, `pre`.`date`
            ORDER BY `pre`.`date` ASC;";
        //echo $sql2;
        $this->count = $GLOBALS['pdo']->query($sql)->fetchColumn();
        $e = $GLOBALS['pdo']->query($sql2)->fetchAll();
        foreach ($e as $es) {
            $entry = New EntryDB($es['id'], $es['camera_id'], $es['stream'], $es['direction'], strtotime($es['date']), $es['count']);
            array_push($this->entries, $entry);
        }
        return [$this];
    }

    public function translate() {
        if ($this->direction === "up") {
            return "haut";
        } else if ($this->direction === "down") {
            return "bas";
        } else if ($this->direction === "left") {
            return "gauche";
        } else if ($this->direction === "right") {
            return "droite";
        }
    }
}
