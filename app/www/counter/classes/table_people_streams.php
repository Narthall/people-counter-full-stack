<?php

class PeopleStreams {
    public $table_name;
    public $id;
    public $camera_id;
    public $stream_address;
    public $entering_direction;
    public $date; // DATE format : YYYY-MM-DD HH:mm:ssdddddzd
    /**
     * Class constructor.
     */
    public function __construct(int $id, $camera_id = NULL, $stream_address = NULL, $entering_direction = NULL, $date = NULL) {
        $this->table_name = "people_streams";
        $this->id = $id;
        if (($camera_id === NULL) || ($stream_address === NULL) || ($entering_direction === NULL) || ($date === NULL)) {
            $sql = $GLOBALS['pdo']->query("SELECT * FROM `" . $this->table_name . "` WHERE `id` = " . $this->id)->fetch();
            $this->camera_id = $sql['camera_id'];
            $this->stream_address = $sql['stream_address'];
            $this->entering_direction = $sql['entering_direction'];
            $this->date = $sql['date'];
        } else {
            $this->fk_people_streams = $camera_id;
            $this->stream_address = $stream_address;
            $this->entering_direction = $entering_direction;
            $this->date = $date;
        }
    }

    public function retrieve_counter_entries(string $direction = "all") {
        $array = [];
        $s = "";
        if ($direction != "all") {$s = " AND `direction` = '" . $direction . "'";}
        $sql = $GLOBALS['pdo']->query("SELECT * FROM `people_counter` WHERE `fk_people_streams` = " . $this->id . $s)->fetchAll();
        foreach ($sql as $row) {
            $array[] = new PeopleCounter($row['id'], $row['fk_people_streams'], $row['direction'], $row['date']);
        }
        return $array;
    }

}
