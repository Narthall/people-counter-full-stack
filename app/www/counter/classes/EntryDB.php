<?php

class EntryDB {
    public $id;
    public $cameraid;
    public $stream;
    public $direction;
    public $date;
    public $count;
    /**
     * Class constructor.
     */
    public function __construct(int $id = NULL, int $cameraid, string $stream, string $direction, int $date, int $count = NULL) {
        $this->id = $id;
        $this->cameraid = $cameraid;
        $this->stream = $stream;
        $this->direction = $direction;
        $this->date = $date;
        $this->count = $count;
    }
}
