<?php
require dirname(__FILE__)."/counter/pdo.php";
require dirname(__FILE__)."/counter/functions.php";
require dirname(__FILE__)."/counter/classes.php";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Préparation d'une requête SQL pour prévenir l'injection
    if ((isset($_POST['range'])) && (isset($_POST['direction']))) {
        $date_array = explode(" - ", $_POST['range']);
        $date_start = $date_array[0] . " 00:00:00";
        $date_end = $date_array[1] . " 23:59:59";
        $dataset = [];
        $people_counter_entries = [];
        
        $hourly_count = count_hourly($date_start, $date_end, $_POST['direction']);
        $entering_directions = [];
        foreach ($hourly_count as $direction => $entries) {
            foreach ($entries as $e) {
                $epoch = $e['date'];
                $dataset[$direction][$epoch] = intval($e['count']);
                ksort($dataset[$direction]);
                foreach (explode(",", $e['fk_people_streams']) as $fk) {
                    $ps = new PeopleStreams($fk);
                    array_push($entering_directions, $ps->entering_direction);
                    $people_counter_entries[$fk] = $ps->retrieve_counter_entries($_POST['direction']);
                }
            }
        }
        if ($_POST['direction'] === "all") {
            foreach ($dataset as $direction => $epoch) {
                foreach ($epoch as $v) {
                    $displayCount += intval($v);
                }
            }
            $display = $displayCount . " personne(s) comptées sur la période sélectionnée.</br>";
        } else {
            $people_count = count_people($date_start, $date_end, $_POST['direction']);
            foreach ($people_count as $direction => $count) {
                if ($count != 0) {
                    $display = $display . $count . " personne(s) " . translate($direction) . " sur la période sélectionnée.</br>";
                }
            }
        }
    } // if range
} // if POST

?>

<!doctype html>
<html lang="fr">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

        <!-- CustomDatePicker -->
        <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
        
        <!-- CDN for Canvas line chart -->
        <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

        <!-- Custom Script -->
        <script src="./index.js"></script>

        <title>People Counter</title>
    </head>
    <body>
        <div class="container">
            <?php
            if (isset($date_array)) {
                echo "<div class='range-data-container' data-name='start' data-value='".$date_array[0]."'></div>";
                echo "<div class='range-data-container' data-name='end' data-value='".$date_array[1]."'></div>";
            }
            ?>
            <h3>Sélectionner une date</h3>
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                <!-- Espace pour calendrier -->
                <div class="input-group form-group form-inline">
                    <input id="range" name="range" type="text" class="form-control pull-right">
                    <div class="form-group">
                        <select class="form-control" id="direction" name="direction">
                            <option value="all" selected>Toutes</option>
                            <option value="in">Entrées</option>
                            <option value="out">Sorties</option>
                        </select>
                    </div>
                    <input type="submit" value="Valider" class="btn float-right btn-success">
                </div>
            </form>

            <?php
                echo $display;
            ?>
            <?php
            if (isset($dataset)) {
                foreach ($dataset as $direction=>$values) {
                    echo "<div class='chart-data-container' data-name='".translate($direction)."' data-contains='".json_encode($values)."'></div>";
                }
            }
            ?>
            <div id="chartContainer" style="height: 300px; width: 100%;"></div>
            <?php
            if (isset($people_counter_entries)) {
                $i = 1;
                foreach ($people_counter_entries as $stream_id=>$count_ids) {
                    $s = new PeopleStreams($stream_id);
                    foreach ($count_ids as $count_id) {
                        if ($i === 1) {echo '<div class="row">';}
                        echo '
                            <div class="col">
                                <img class="img-thumbnail rounded" src="'.$count_id->image.'" alt="Photo Indisponible">
                                <div class="desc">'.$count_id->date.' : '.translate($count_id->direction).'</div>
                            </div>';
                        if ($i === 4) {echo '</div>'; $i = 0;}
                        ++$i;
                    }
                }
                for ($i = $i; $i <= 4; $i++) { echo "<div class='col'></div>"; if ($i === 4) {echo '</div>';}}
            }
            ?>
        </div>

    </body>
</html>