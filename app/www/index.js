$(document).ready(function() {
    datePickerStart = ($('.range-data-container[data-name="start"]').length ? $('.range-data-container[data-name="start"]').data('value') : moment());
    datePickerEnd = ($('.range-data-container[data-name="end"]').length ? $('.range-data-container[data-name="end"]').data('value') : moment());
    $('#range').daterangepicker(
        {
            "locale": {
                "format": "YYYY-MM-DD",
                "separator": " - ",
                "applyLabel": "Confirmer",
                "cancelLabel": "Annuler",
                "fromLabel": "Du",
                "toLabel": "Au",
                "customRangeLabel": "Personnalisé",
                "weekLabel": "S",
                "daysOfWeek": [
                    "Dim",
                    "Lun",
                    "Mar",
                    "Mer",
                    "Jeu",
                    "Ven",
                    "Sam"
                ],
                "monthNames": [
                    "Janvier",
                    "Février",
                    "Mars",
                    "Avril",
                    "Mai",
                    "Juin",
                    "Juillet",
                    "Août",
                    "Septembre",
                    "Octobre",
                    "Novembre",
                    "Décembre"
                ],
                "firstDay": 1
            },
            ranges: {
               "Aujourdh'ui": [moment(), moment()],
               "Hier": [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
               "7 derniers jours": [moment().subtract(6, 'days'), moment()],
               "30 derniers jours": [moment().subtract(29, 'days'), moment()],
               "Ce mois": [moment().startOf('month'), moment().endOf('month')],
               "Mois dernier": [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "startDate": datePickerStart,
            "endDate": datePickerEnd,
            "showCustomRangeLabel": true,
            "alwaysShowCalendars": true
        },
        function(start, end, label) {
            console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
        }
    );
    
    var values = [];
    $('.chart-data-container').each(function() {
        var container = $(this);
        var contains = container.data('contains');
        var coordinates = [];
        //console.log(contains);
        Object.keys(contains).forEach(key => {
            // Epoch date from MySQL is already in GMT+2 (France). JS automatically adds 2h to epoch, so we have to remove them.
            var x = new Date(((key - (3600 * 2)) * 1000));
            coordinates.push({ x:  x, y: Number(contains[key]) });
        });
        var dataset = {
            name: container.data('name'),
            dataPoints: coordinates,
            type: "spline",
            yValueFormatString: "#0.##",
            showInLegend: true,
        }
        values.push(dataset);
    });
    
    var chart = new CanvasJS.Chart("chartContainer", {
        animationEnabled: true,
        title:{
            text: "Personnes comptées"
        },
        axisX: {
            valueFormatString: "DD/MM/YYYY HH'h'"
        },
        axisY: {
            title: "Nombre de personnes comptées",
            includeZero: true
        },
        legend:{
            cursor: "pointer",
            fontSize: 16,
            itemclick: toggleDataSeries
        },
        toolTip:{
            shared: true,
            contentFormatter: function (e) {
                var date = e.entries[0].dataPoint.x;
                var weekday;
                switch (date.getDay()) {
                    case 0:
                        weekday = "Dim"
                        break;
                    case 1:
                        weekday = "Lun"
                        break;
                    case 2:
                        weekday = "Mar"
                        break;
                    case 3:
                        weekday = "Mer"
                        break;
                    case 4:
                        weekday = "Jeu"
                        break;
                    case 5:
                        weekday = "Ven"
                        break;
                    case 6:
                        weekday = "Sam"
                        break;
                }
                var content = weekday + ' ' +
                    ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '/' +
                    ((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '/' +
                    date.getFullYear() + " à " +
                    ((date.getHours() > 9) ? date.getHours() : ('0' + date.getHours())) + "h<br/>";
				for (var i = 0; i < e.entries.length; i++) {
                    content += "<span style='color:" + e.entries[i].dataSeries.lineColor + ";'>" + e.entries[i].dataSeries.name + " : </span>" +
                        "<strong>" + e.entries[i].dataPoint.y + "</strong>";
					content += "<br/>";
				}
				return content;
			}
        },
        data: values
    });
    chart.render();
    
    function toggleDataSeries(e){
        if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        }
        else{
            e.dataSeries.visible = true;
        }
        chart.render();
    }
})